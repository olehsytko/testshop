<?php

use yii\db\Migration;

/**
 * Handles the creation of table `category`.
 */
class m190124_114159_create_category_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(10)->unsigned(),
            'parent_id' => $this->integer(10)->unsigned()->notNull()->defaultValue(0),
            'name' => $this->string(255)->notNull(),
            'keywords' => $this->string(255)->defaultValue(NULL),
            'description' => $this->string(255)->defaultValue(NULL),

        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

        $this->batchInsert('category', [ 'parent_id', 'name', 'keywords', 'description'  ],
            [
                [0, 'Sportswear', NULL, NULL],
                [0, 'Mens', NULL, NULL],
                [0, 'Womens', NULL, NULL],
                [1, 'Nike', NULL, NULL],
                [1, 'Under Armour', NULL, NULL],
                [1, 'Adidas', NULL, NULL],
                [1, 'Puma', NULL, NULL],
                [1, 'ASICS', NULL, NULL],
                [2, 'Fendi', NULL, NULL],
                [2, 'Guess', NULL, NULL],
                [2, 'Valentino', NULL, NULL],
                [2, 'Dior', NULL, NULL],
                [2, 'Versace', NULL, NULL],
                [2, 'Armani', NULL, NULL],
                [2, 'Prada', NULL, NULL],
                [2, 'Dolce and Gabbana', NULL, NULL],
                [2, 'Chanel', NULL, NULL],
                [2, 'Gucci', NULL, NULL],
                [3,'Fendi',NULL, NULL],
                [3,'Guess',NULL, NULL],
                [3,'Valentino',NULL, NULL],
                [3,'Dior',NULL, NULL],
                [3,'Versace',NULL, NULL],
                [0, 'Kids', NULL, NULL],
                [0, 'Fashion', NULL, NULL],
                [0, 'Households', NULL, NULL],
                [0, 'Interiors', NULL, NULL],
                [0, 'Clothing', NULL, NULL],
                [0, 'Bags', 'сумки ключевики...', 'сумки описание...'],
                [0, 'Shoes', NULL, NULL],
            ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('category');
    }
}
