<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 25.01.19
 * Time: 11:16
 */

namespace app\models;

use yii\db\ActiveRecord;


class Product extends ActiveRecord{

    public static function tableName()
    {
        return "product";
    }

    public function getCategory(){
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

}