<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 25.01.19
 * Time: 11:11
 */

namespace app\models;

use yii\db\ActiveRecord;


class Category extends ActiveRecord{

   public static function tableName()
   {
       return "category";
   }

    public function getProducts(){
        return $this->hasMany(Product::className(), ['category_id' => 'id']);
    }
}