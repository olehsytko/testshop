<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 12.02.19
 * Time: 20:21
 */

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use Yii;
use yii\web\HttpException;

class ProductController extends AppController
{

    public function actionIndex(){

        return $this->render('index');
    }

    public function actionView($id){

        $id = Yii::$app->request->get('id');
        $product = Product::findOne($id);

        if(empty($product)){
            throw new HttpException(404,'Good could not be found');
        }

        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();

        $this->setMeta('E_SHOPPER | '.$product->name, $product->keywords, $product->description);

        return $this->render('view', ['product' => $product, 'hits' => $hits]);
    }
    
    

}