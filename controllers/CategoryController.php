<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 28.01.19
 * Time: 13:42
 */

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use Yii;
use yii\data\Pagination;
use yii\web\HttpException;


class CategoryController extends AppController{

    public function actionIndex(){

        $hits = Product::find()->where(['hit' => '1'])->limit(7)->all();
        $this->setMeta('E_SHOPPER');
        return  $this->render('index', ['hits' => $hits]);
    }

    public function actionView($alias){
        $name = Yii::$app->request->get('alias');
        $cat = Category::findOne(['name' => $name]);
        $id = $cat['id'];

        $category = Category::findOne($id);

        if(empty($category)){
            throw new HttpException(404,"The category could not be found");
        }

       // $products = Product::find()->where(['category_id' => $id])->all();
        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 1, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
        $this->setMeta('E_SHOPPER | '.$category->name, $category->keywords, $category->description);
        return $this->render('view',['products' => $products, 'pages' => $pages, 'category' => $category]);
    }

    public function actionSearch(){
        $q = Yii::$app->request->get('sech');

        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6, 'forcePageParam' => false, 'pageSizeParam' => false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();
        $this->setMeta('E_SHOPPER | '.$q);

        return $this->render('search',['products' => $products, 'pages' => $pages, 'q' => $q]);
    }
}