<?php
/**
 * Created by PhpStorm.
 * User: dimple
 * Date: 14.02.19
 * Time: 12:08
 */

namespace app\controllers;

use app\models\Product;
use app\models\Category;
use app\models\Cart;
use app\models\Order;
use app\models\OrderItem;
use Yii;


class CartController extends AppController
{

    public function actionAdd(){
        $id = Yii::$app->request->get('id');
        $qty = Yii::$app->request->get('qty');
        if(empty($qty)){
            $qty = 1;
        }
        //echo $id;

        $product = Product::findOne($id);

        if(empty($product)){
            return false;
        }

        $session = Yii::$app->session;
        $session->open();

        $cart = new Cart();
        $cart->addToCart($product, $qty);

        if(!Yii::$app->request->isAjax){
            return $this->redirect(Yii::$app->request->referrer);
        }
        $this->layout = false;
        return $this->render('cart-modal',['session' => $session]);

    }

    public function actionDel(){
        $id = Yii::$app->request->get('id');

        $session = Yii::$app->session;
        $session->open();

        $cart = new Cart();
        $cart->delFromCart($id);

        $this->layout = false;

        return $this->render('cart-modal',['session' => $session]);

    }

    public function actionView(){
//        Yii::$app->mailer->compose()
//            ->setFrom('bender250222@gmail.com')
//            ->setTo('bender250222@gmail.com')
//            ->setSubject('Тема сообщения')
//            ->setTextBody('Текст сообщения')
//            ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
//            ->send();

        $session = Yii::$app->session;
        $session->open();

        $this->setMeta('Cart');

        $order = new Order();
        if($order->load(Yii::$app->request->post())){
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            if($order->save()){
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success', 'Ваш заказ принят! Скоро вам позвонят.');
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');

                return $this->refresh();
            } else {
                Yii::$app->session->setFlash('error', 'Ошибка выполнение заказа');
            }
        }
        return $this->render('view', compact('session', 'order'));
    }

    protected function saveOrderItems($items, $order_id){
        foreach ($items as $id => $item) {
            $order_items = new OrderItem();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save();
        }
    }

    public function actionClear(){
        $session = Yii::$app->session;
        $session->open();
        $session->remove('cart');
        $session->remove('cart.qty');
        $session->remove('cart.sum');

        $this->layout = false;

        return $this->render('cart-modal',['session' => $session]);
    }


}